package ru.volnenko.se.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

@RestController
@RequestMapping("/project")
public class ProjectResource {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/{id}")
    public Project get(@PathVariable("id") String id) {
        return projectService.getOne(id);
    }

    @PostMapping
    public Project post(@RequestBody Project project) {
        return projectService.save(project);
    }

    @PutMapping
    public void put(@RequestBody Project project) {
        projectService.save(project);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        projectService.deleteById(id);
    }

}
