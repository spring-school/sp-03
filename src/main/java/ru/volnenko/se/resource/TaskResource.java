package ru.volnenko.se.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Task;

@RestController
@RequestMapping("/task")
public class TaskResource {

    @Autowired
    private ITaskService taskService;

    @GetMapping("/{id}")
    public Task get(@PathVariable("id") String id) {
        return taskService.getOne(id);
    }

    @PostMapping
    public Task post(@RequestBody Task task) {
        return taskService.save(task);
    }

    @PutMapping
    public void put(@RequestBody Task task) {
        taskService.save(task);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        taskService.deleteById(id);
    }

}
