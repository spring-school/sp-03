package ru.volnenko.se.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectListResource {

    @Autowired
    private IProjectService projectService;

    @GetMapping
    public List<Project> get() {
        return projectService.findAll();
    }

    @PostMapping
    public List<Project> post(@RequestBody List<Project> projects) {
        return projectService.saveAll(projects);
    }

    @PutMapping
    public void put(@RequestBody List<Project> projects) {
        projectService.saveAll(projects);
    }

    @DeleteMapping
    public void delete() {
        projectService.deleteAll();
    }

}
