package ru.volnenko.se.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Task;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskListResource {

    @Autowired
    private ITaskService taskService;

    @GetMapping
    public List<Task> get() {
        return taskService.findAll();
    }

    @PostMapping
    public List<Task> post(@RequestBody List<Task> tasks) {
        return taskService.saveAll(tasks);
    }

    @PutMapping
    public void put(@RequestBody List<Task> tasks) {
        taskService.saveAll(tasks);
    }

    @DeleteMapping
    public void delete() {
        taskService.deleteAll();
    }

}
