package ru.volnenko.se.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.volnenko.se.entity.Project;

import java.util.List;


@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @Override
    Project getOne(String s);

    @Override
    List<Project> findAll();

    @Override
    <S extends Project> S save(S s);

    @Override
    <S extends Project> List<S> saveAll(Iterable<S> iterable);

    @Override
    void deleteById(String s);

    @Override
    void deleteAll();

}
