package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Project;

import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface IProjectService {

    Project getOne(String s);

    List<Project> findAll();

    Project createProject();

    <S extends Project> S save(S s);

    <S extends Project> List<S> saveAll(Iterable<S> iterable);

    void deleteById(String s);

    void deleteAll();

}
