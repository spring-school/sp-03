package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.TaskRepository;

import java.util.List;

/**
 * @author Denis Volnenko
 */

@Service
@Transactional
public class TaskService implements ru.volnenko.se.api.service.ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Task getOne(String s) {
        return taskRepository.getOne(s);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task createTask() {
        final Task task = new Task();
        return taskRepository.save(task);
    }

    @Override
    public <S extends Task> S save(S s) {
        return taskRepository.save(s);
    }

    @Override
    public <S extends Task> List<S> saveAll(Iterable<S> iterable) {
        return taskRepository.saveAll(iterable);
    }

    @Override
    public void deleteById(String s) {
        taskRepository.deleteById(s);
    }

    @Override
    public void deleteAll() {
        taskRepository.deleteAll();
    }

}
