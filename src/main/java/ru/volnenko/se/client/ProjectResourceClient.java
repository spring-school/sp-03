package ru.volnenko.se.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.volnenko.se.entity.Project;

import java.util.Date;

public class ProjectResourceClient {

    private static final String URL = "http://localhost:8080/project/";

    public static Project get(String id) {
        String url = URL + "{id}";
        final RestTemplate template = new RestTemplate();
        return template.getForObject(url, Project.class, id); //3-й аргумент id подставится в String url
    }

    public static Project post(Project project) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<Project> entity = new HttpEntity<>(project, headers);
        return template.postForObject(URL, entity, Project.class);
    }

    public static void put(Project project) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<Project> entity = new HttpEntity<>(project, headers);
        template.put(URL, entity);
    }

    public static void delete(String id) {
        String url = URL + "{id}";
        final RestTemplate template = new RestTemplate();
        template.delete(url, id); //2-й аргумент id подставится в String url
    }

    public static void main(String[] args) {
        Project project = get("8ed8bfae-ed71-4601-8fc8-5a18e295e39d");
        System.out.println(project.getId());
        System.out.println(project.getName());
        System.out.println("--------------------------------------------");
        System.out.println();

        project.setDateEnd(new Date());
        Project newProject = post(project);
        System.out.println(newProject.getId());
        System.out.println(newProject.getDateEnd());
        System.out.println("--------------------------------------------");
        System.out.println();

        Project projectFromGetMethod = get("8ed8bfae-ed71-4601-8fc8-5a18e295e39d");
        projectFromGetMethod.setDateBegin(new Date(2000000));
        projectFromGetMethod.setDateEnd(new Date());
        put(projectFromGetMethod);
        System.out.println(projectFromGetMethod.getId());
        System.out.println(projectFromGetMethod.getDateBegin());
        System.out.println(projectFromGetMethod.getDateEnd());
    }
}
