package ru.volnenko.se.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.volnenko.se.entity.Project;

import java.util.Arrays;
import java.util.List;

public class ProjectListResourceClient {

    private static final String URL = "http://localhost:8080/projects/";

    public static List<Project> get() {
        final RestTemplate template = new RestTemplate();
        final Project[] result = template.getForObject(URL, Project[].class);
        if (result == null) return null;
        return Arrays.asList(result);
    }

    public static List<Project> post(List<Project> projects) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<List<Project>> entity = new HttpEntity<>(projects, headers);
        Project[] result = template.postForObject(URL, entity, Project[].class);
        if (result == null) return null;
        return Arrays.asList(result);
    }

    public static void put(List<Project> projects) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<List<Project>> entity = new HttpEntity<>(projects, headers);
        template.put(URL, entity);
    }

    public static void delete() {
        final RestTemplate template = new RestTemplate();
        template.delete(URL);
    }

    public static void main(String[] args) {
        delete();
    }
}
