package ru.volnenko.se.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.volnenko.se.entity.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskListResourceClient {

    private static final String URL = "http://localhost:8080/tasks/";

    public static List<Task> get() {
        final RestTemplate template = new RestTemplate();
        final Task[] result = template.getForObject(URL, Task[].class);
        if (result == null) return null;
        return Arrays.asList(result);
    }

    public static List<Task> post(List<Task> tasks) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<List<Task>> entity = new HttpEntity<>(tasks, headers);
        Task[] result = template.postForObject(URL, entity, Task[].class);
        if (result == null) return null;
        return Arrays.asList(result);
    }

    public static void put(List<Task> tasks) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<List<Task>> entity = new HttpEntity<>(tasks, headers);
        template.put(URL, entity);
    }

    public static void delete() {
        final RestTemplate template = new RestTemplate();
        template.delete(URL);
    }

    public static void main(String[] args) {

        List<Task> list = new ArrayList<>();
        Task evil = new Task();
        evil.setName("from dusk");
        list.add(evil);
        post(list);
    }
}
