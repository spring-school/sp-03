package ru.volnenko.se.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.volnenko.se.entity.Task;

import java.util.Date;

public class TaskResourceClient {

    private static final String URL = "http://localhost:8080/task/";

    public static Task get(String id) {
        String url = URL + "{id}";
        final RestTemplate template = new RestTemplate();
        return template.getForObject(url, Task.class, id); //3-й аргумент id подставится в String url
    }

    public static Task post(Task task) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<Task> entity = new HttpEntity<>(task, headers);
        return template.postForObject(URL, entity, Task.class);
    }

    public static void put(Task task) {
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<Task> entity = new HttpEntity<>(task, headers);
        template.put(URL, entity);
    }

    public static void delete(String id) {
        String url = URL + "{id}";
        final RestTemplate template = new RestTemplate();
        template.delete(url, id); //2-й аргумент id подставится в String url
    }

    public static void main(String[] args) {
        Task task = get("59ec538e-e671-462c-8338-8547f396eead");
        System.out.println(task.getId());
        System.out.println(task.getName());
        System.out.println("--------------------------------------------");
        System.out.println();

        Task taskFromGetMethod = get("59ec538e-e671-462c-8338-8547f396eead");
        taskFromGetMethod.setDateBegin(new Date(2000000));
        taskFromGetMethod.setDateEnd(new Date());
        put(taskFromGetMethod);
        System.out.println(taskFromGetMethod.getId());
        System.out.println(taskFromGetMethod.getDateBegin());
        System.out.println(taskFromGetMethod.getDateEnd());
    }
}
