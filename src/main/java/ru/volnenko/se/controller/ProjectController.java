package ru.volnenko.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public final class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/project-list")
    public String listProjectGet(Model model) {
        final List<Project> projects = projectService.findAll();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping(value = "/project-create")
    public String addProjectGet() {
        final Project project = new Project();
        project.setName("new");
        projectService.save(project);
        return "redirect:/project-list";
    }

    @GetMapping(value = "/project-edit/{id}")
    public String editProjectGet(
            Model model,
            @PathVariable("id") String id
    ) {
        final Project project = projectService.getOne(id);
        if (project != null) {
            model.addAttribute("project", project);
        }
        return "project-edit";
    }

    @PostMapping(value = "/project-save")
    public String saveProject(
            @ModelAttribute("project") Project project,
            BindingResult result
    ) {
        if (!result.hasErrors()) {
            projectService.save(project);
        }
        return "redirect:/project-list";
    }

    @GetMapping(value="/project-delete/{id}")
    public String deleteProject(@PathVariable("id") String id) {
        projectService.deleteById(id);
        return "redirect:/project-list";
    }

}
