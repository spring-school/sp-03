package ru.volnenko.se.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public final class MainController {

    @GetMapping("/")
    public final String index() {
        return "index";
    }

}
