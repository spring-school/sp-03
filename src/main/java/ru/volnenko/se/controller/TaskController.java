package ru.volnenko.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Task;

import java.util.List;

@Controller
public final class TaskController {

    @Autowired
    private ITaskService taskService;

    @GetMapping(value = "/task-list")
    public String listTaskGet(Model model) {
        final List<Task> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping(value = "/task-create")
    public String addTaskGet() {
        final Task task = new Task();
        task.setName("new");
        taskService.save(task);
        return "redirect:/task-list";
    }

    @GetMapping(value = "/task-edit/{id}")
    public String editTaskGet(
            Model model,
            @PathVariable("id") String id
    ) {
        final Task task = taskService.getOne(id);
        if (task != null) {
            model.addAttribute("task", task);
        }
        return "task-edit";
    }

    @PostMapping(value = "/task-save")
    public String saveTask(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        if (!result.hasErrors()) {
            taskService.save(task);
        }
        return "redirect:/task-list";
    }

    @GetMapping(value = "/task-delete/{id}")
    public String deleteTask(@PathVariable("id") String id) {
        taskService.deleteById(id);
        return "redirect:/task-list";
    }

}
