<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>UPDATE PROJECT</title>
</head>
<body>
<form:form method="post" action="/project-save" modelAttribute="project">
    <form:hidden path="id" />
    <p>
        <div style="...">NAME</div>
        <form:input path="name" />
    </p>
    <p>
        <div style="...">DATE BEGIN</div>
        <form:input type="date" path="dateBegin" />
    </p>
    <p>
        <div style="...">DATE END</div>
        <form:input type="date" path="dateEnd" />
    </p>

    <p>
        <button type="submit">Сохранить</button>
    </p>
</form:form>
</body>
</html>